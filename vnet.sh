# Création de mon réseau Vnet sur Azure

# Déclaration des variables

vnetName=Vnet_OCC_ASD_Jacques
resourceGroup=OCC_ASD_JACQUES
networkAddressPrefix="10.0.11.0/24"
subnetName="Subnet_Vnet_OCC_ASD_Jacques_"
subnetAddressPrefix="10.0.11."
i=1
x=16
subnetNetmask="/28"
admin="admin"

az network vnet create \
    --name $vnetName \
    --resource-group $resourceGroup \
    --address-prefix $networkAddressPrefix \
    --subnet-name $subnetName$admin \
    --subnet-prefix $subnetAddressPrefix"0"$subnetNetmask

while [ $i -ne 16 ]
do
  az network vnet subnet create \
      --name $subnetName$i \
      --resource-group $resourceGroup \
      --vnet-name $vnetName \
      --address-prefix $subnetAddressPrefix$x$subnetNetmask
  x=$((x+16))
  i=$((i+1))
done
